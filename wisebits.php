<?php

/**
 * Class WeeklyServiceTraffic
 * 
 * Формирует отчет по почтовому трафику.
 */
class WeeklyServiceTraffic
{
    protected $_report = [
        'detail' => [
            'byServicesLast5weeks' => [],
            'byGender' => [],
            'byPayment' => [],
            'byActivities' => [],
            'byMail' => []
        ]
    ];

    protected $_services = null;
    protected $_mails = [];
    protected $_serviceOrder = [];
    protected $_mailsOrder = [];

    protected $_filter = [];

    protected $_activities = [
        Stat_Service_User::AUDIENCE_ACT_LOW_ACTIVE,
        Stat_Service_User::AUDIENCE_ACT_ACTIVE,
        Stat_Service_User::AUDIENCE_ACT_SUPER_ACTIVE,
        Stat_Service_User::AUDIENCE_ACT_INSTALLS,
        Stat_Service_User::AUDIENCE_ACT_NEWCOMER,
        Stat_Service_User::AUDIENCE_ACT_RETURN_FIRST,
        Stat_Service_User::AUDIENCE_ACT_RETURN,
        Stat_Service_User::AUDIENCE_ACT_LOST,
    ];

    protected $_fields = [
        Base_Mailer_Service_Stats::MAIL_SEND_COUNT,
        Base_Mailer_Service_Stats::MAIL_CLICK_COUNT,
        Base_Mailer_Service_Stats::MAIL_FILTERED,
    ];

    public function __construct($services)
    {
        $this->_services = $services;
    }

    public function setFilter($field, $value)
    {
        $this->_filter[$field] = $value;
    }

    public function getServices()
    {
        return $this->_services;
    }

    /**
     * return service ids ordering by "send" value. Available after generate report for admin
     * @return array
     */
    public function getServicesOrder()
    {
        return $this->_serviceOrder;
    }

    /**
     * return mails ids ordering by "send" value. Available after generate report by services
     * @return array
     */
    public function getMailsOrder()
    {
        return $this->_mailsOrder;
    }

    public function getMails($serviceId = null)
    {
        if($serviceId === null){
            return $this->_mails;
        }
        if(!array_key_exists($serviceId, $this->_mails)){
            $this->_mails[$serviceId] = MailSystem_Model_Mail::model()->getMailsByService($serviceId, false);
        }
        return $this->_mails[$serviceId];
    }

    /**
     * Return data for render report
     * @param $inputServices
     * @return array
     */
    public function getIndividualReport($inputServices)
    {
        if(empty($inputServices)){
            return $this->_getAdminReport();
        }
        $showServices = [];
        foreach($inputServices as $serviceId){
            if(empty($serviceId)){
                return $this->_getAdminReport();
            }
            if(!isset($this->_services[$serviceId])){
                continue;
            }
            $showServices[] = $serviceId;
        }
        return $this->_getServicesReport($showServices);
    }

    protected function _getSortedValue($stats)
    {
        if(!isset($stats)){
            return 0;
        }
        $i = 0;
        foreach($stats as $weekData){
            if($i === 1){
                if(isset($weekData[Base_Mailer_Service_Stats::MAIL_SEND_COUNT][0])){
                    return $weekData[Base_Mailer_Service_Stats::MAIL_SEND_COUNT][0];
                } else {
                    break;
                }
            }
            ++$i;
        }
        return 0;
    }

    /**
     * Return data for admins report
     * @return array
     */
    protected function _getAdminReport()
    {
        $end = new \DateTime();
        $end->setTimestamp(TIME - (date('N') * 86400));
        $di = new \DateInterval('P13D');
        $di->invert = 1;
        $start = clone $end;
        $start->add($di);

        $date5weeksStart = clone $end;
        $di5weeks = new \DateInterval('P34D');
        $di5weeks->invert = 1;
        $date5weeksStart->add($di5weeks);

        if(!array_key_exists('byServicesLast2weeks', $this->_report['detail'])){
            $reports = [];
            $servOrdVal = [];
            foreach($this->_services as $service) {
                $reports[$service->id] = $this->_getStatsByServices($start, $end, [$service->id]);
                $servOrdVal[$service->id] = $this->_getSortedValue($reports[$service->id]);
            }
            $this->_report['detail']['byServicesLast2weeks'] = $reports;
            arsort($servOrdVal);
            $this->_serviceOrder = array_keys($servOrdVal);
        }

        if(!array_key_exists('common', $this->_report)){
            $this->_report['common'] = [
                'byServices'   => $this->_getStatsByServices($date5weeksStart, $end),
                'byActivities' => $this->_getStatsByActivities($start, $end),
                'byGender'     => $this->_getStatsByGender($start, $end),
                'byPayment'    => $this->_getStatsByPayment($start, $end),
            ];
        }

        return [
            'common' => $this->_report['common'],
            'detail' => [
                'byServices' => $this->_report['detail']['byServicesLast2weeks'],
            ],
        ];
    }

    /**
     * return data by selected services
     * @param $showServices
     * @return array
     */
    protected function _getServicesReport($showServices)
    {
        $end = new \DateTime();
        $end->setTimestamp(TIME - (date('N') * 86400));
        $di = new \DateInterval('P13D');
        $di->invert = 1;
        $start = clone $end;
        $start->add($di);

        $date5weeksStart = clone $end;
        $di5weeks = new \DateInterval('P34D');
        $di5weeks->invert = 1;
        $date5weeksStart->add($di5weeks);

        $result = [
            'detail'=> [
                'byServices' => [],
                'byMail' => [],
                'byPayment' => [],
                'byGender' => [],
                'byActivities' => []
            ]
        ];
        foreach($showServices as $serviceId){
            if(!isset($this->_services[$serviceId])){
                $this->_report['detail']['byServicesLast5weeks'][$serviceId] = [];
                $this->_report['detail']['byActivities'][$serviceId] = [];
                $this->_report['detail']['byGender'][$serviceId] = [];
                $this->_report['detail']['byPayment'][$serviceId] = [];
                $this->_report['detail']['byMail'][$serviceId] = [];
                $this->_mails[$serviceId] = [];
                $this->_mailsOrder[$serviceId] = [];
            }

            if(!array_key_exists($serviceId, $this->_report['detail']['byServicesLast5weeks'])){
                $this->_report['detail']['byServicesLast5weeks'][$serviceId] = $this->_getStatsByServices($date5weeksStart, $end, [$serviceId]);
            }
            if(!array_key_exists($serviceId, $this->_report['detail']['byGender'])){
                $this->_report['detail']['byGender'][$serviceId] = $this->_getStatsByGender($start, $end, [$serviceId]);
            }
            if(!array_key_exists($serviceId, $this->_report['detail']['byPayment'])){
                $this->_report['detail']['byPayment'][$serviceId] = $this->_getStatsByPayment($start, $end, [$serviceId]);
            }
            if(!array_key_exists($serviceId, $this->_report['detail']['byActivities'])){
                $this->_report['detail']['byActivities'][$serviceId] = $this->_getStatsByActivities($start, $end, [$serviceId]);
            }

            if(!array_key_exists($serviceId, $this->_mails)){
                $this->_mails[$serviceId] = MailSystem_Model_Mail::model()->getMailsByService($serviceId, false);
            }

            if(!array_key_exists($serviceId, $this->_report['detail']['byMail'])){
                $this->_report['detail']['byMail'][$serviceId] = [];
                if(is_array($this->_mails[$serviceId])){
                    $mailOrdVal = [];
                    foreach($this->_mails[$serviceId] as $mail){
                        if($mail->disabled){
                            continue;
                        }
                        $stats = $this->_getStatsByMail($start, $end, $mail->id);
                        if($this->_hasStat($stats)){
                            $mailOrdVal[$mail->id] = $this->_getSortedValue($stats);
                            $this->_report['detail']['byMail'][$serviceId][$mail->id] = $stats;
                        }
                    }
                    arsort($mailOrdVal);
                    $this->_mailsOrder[$serviceId] = array_keys($mailOrdVal);
                } else {
                    $this->_report['detail']['byMail'][$serviceId] = [];
                    $this->_mailsOrder[$serviceId] = [];
                }
            }

            $result['detail']['byServices'][$serviceId]   = $this->_report['detail']['byServicesLast5weeks'][$serviceId];
            $result['detail']['byActivities'][$serviceId] = $this->_report['detail']['byActivities'][$serviceId];
            $result['detail']['byGender'][$serviceId]     = $this->_report['detail']['byGender'][$serviceId];
            $result['detail']['byPayment'][$serviceId]    = $this->_report['detail']['byPayment'][$serviceId];
            $result['detail']['byMail'][$serviceId]       = $this->_report['detail']['byMail'][$serviceId];
        }
        return $result;
    }

    /**
     * Return data grouping by services or dates
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param array $serviceIds
     * @return array
     *      если false, то по неделям
     */
    protected function _getStatsByServices(\DateTime $dateStart, \DateTime $dateEnd, $serviceIds = null)
    {
        if($serviceIds === null){
            $search = $this->_filter;
        } else {
            $search = array_merge([
                'issue' => [
                    'field' => Mail_Model_Statistics::WHERE_VAR_SERVICE_ID,
                    'value' => $serviceIds
                ]
            ], $this->_filter);
        }

        $stats = new Mail_Model_Statistics($this->_fields, $dateStart->format('d/m/Y'), $dateEnd->format('d/m/Y'), $search, false, true);

        return $this->_combineByWeek($stats->getResult());
    }

    /**
     * Return data by mail
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param $mailId
     * @return array результат сгруппирован по неделям
     */
    protected function _getStatsByMail(\DateTime $dateStart, \DateTime $dateEnd, $mailId)
    {
        $search = array_merge([
            'issue' => [
                'field' => Mail_Model_Statistics::WHERE_VAR_MAIL_ID,
                'value' => [$mailId]
            ]
        ], $this->_filter);
        $stats = new Mail_Model_Statistics($this->_fields, $dateStart->format('d/m/Y'), $dateEnd->format('d/m/Y'), $search, false, true);

        return $this->_combineByWeek($stats->getResult());
    }

    /**
     * Check stats. If not exists, return false
     * @param $stats
     * @return bool
     */
    protected function _hasStat($stats)
    {
        foreach($stats as $weekData){
            if(!isset($weekData[Base_Mailer_Service_Stats::MAIL_SEND_COUNT][0])){
                continue;
            }
            if($weekData[Base_Mailer_Service_Stats::MAIL_SEND_COUNT][0] > 0){
                return true;
            }
        }
        return false;
    }

    /**
     * Return data by activity
     *
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param null $serviceIds
     * @return array result grouped by activity, then weeks
     */
    protected function _getStatsByActivities(\DateTime $dateStart, \DateTime $dateEnd, $serviceIds = null)
    {
        $activities = $this->_activities;

        if($serviceIds === null){
            $searchBase = [];
        } else {
            $searchBase = [
                'issue' => [
                    'field' => Mail_Model_Statistics::WHERE_VAR_SERVICE_ID,
                    'value' => $serviceIds
                ]
            ];
        }

        $result = [];
        foreach ($activities as $activityId) {
            $search = array_merge($searchBase, [
                Mail_Model_Statistics::WHERE_VAR_ACTIVITY_ID => $activityId
            ]);
            $stats = new Mail_Model_Statistics($this->_fields, $dateStart->format('d/m/Y'), $dateEnd->format('d/m/Y'), $search, false, true);
            $result[$activityId] = $this->_combineByWeek($stats->getResult());
        }

        return $result;
    }

    protected function _getStatsByGender(\DateTime $dateStart, \DateTime $dateEnd, $serviceIds = null)
    {
        $genders = array_keys(Stat_Service_User::getAudienceValues('gen', true, true));

        if($serviceIds === null){
            $searchBase = $this->_filter;
        } else {
            $searchBase = array_merge([
                'issue' => [
                    'field' => Mail_Model_Statistics::WHERE_VAR_SERVICE_ID,
                    'value' => $serviceIds
                ]
            ], $this->_filter);
        }

        $result = [];
        foreach ($genders as $gender) {
            $search = array_merge($searchBase, [
                Mail_Model_Statistics::WHERE_VAR_GENDER_ID => $gender
            ]);
            $stats = new Mail_Model_Statistics($this->_fields, $dateStart->format('d/m/Y'), $dateEnd->format('d/m/Y'), $search, false, true);
            $result[$gender] = $this->_combineByWeek($stats->getResult());
        }

        return $result;
    }

    protected function _getStatsByPayment(\DateTime $dateStart, \DateTime $dateEnd, $serviceIds = null)
    {
        $slice = array_keys(Billing_Service_History::getPayGroupNames());

        if($serviceIds === null){
            $searchBase = $this->_filter;
        } else {
            $searchBase = array_merge([
                'issue' => [
                    'field' => Mail_Model_Statistics::WHERE_VAR_SERVICE_ID,
                    'value' => $serviceIds
                ]
            ], $this->_filter);
        }

        $result = [];
        foreach ($slice as $item) {
            $search = array_merge($searchBase, [
                Mail_Model_Statistics::WHERE_VAR_PAYMENT_ID => $item
            ]);
            $stats = new Mail_Model_Statistics($this->_fields, $dateStart->format('d/m/Y'), $dateEnd->format('d/m/Y'), $search, false, true);
            $result[$item] = $this->_combineByWeek($stats->getResult());
        }

        return $result;
    }

    /**
     * Grouped stats by days to stats by weeks
     * @param array $statsByDays
     * @return array
     */
    protected function _combineByWeek(array $statsByDays)
    {
        $statsByWeek = [];
        $weekData = [];
        $day = 0;

        $firstDay = null;

        foreach ($statsByDays as $date => $stat) {
            if($firstDay === null){
                $firstDay = $date;
            }
            foreach($stat as $field => $data){
                if(in_array($field, $this->_fields, true)){
                    continue;
                }
                unset($stat[$field]);
            }
            $this->_array_rec_sum($weekData, $stat);
            ++$day;
            if (($day % 7) === 0) {
                $statsByWeek[$firstDay . ' - ' . $date] = $weekData;
                $weekData = [];
                $firstDay = null;
            }
        }
        if($firstDay !== null && isset($date)){
            $statsByWeek[$firstDay . ' - ' . $date] = $weekData;
        }

        return $statsByWeek;
    }

    /**
     * Modify first array - add to every item second array to first array
     * @param $array
     * @param $arrayAdd
     */
    protected function _array_rec_sum(array &$array, array $arrayAdd)
    {
        foreach($arrayAdd as $index => $value){
            if(!isset($array[$index])){
                $array[$index] = $value;
                continue;
            }

            if(!is_array($value)){
                $array[$index]+= $value;
                continue;
            }

            $this->_array_rec_sum($array[$index], $value);
        }
    }
}


// usage
$services = [1,2,3];
$report = new WeeklyServiceTraffic($services);
$result1 = $report->getIndividualReport([1]);
$result2 = $report->getIndividualReport([2,3]);
$result3 = $report->getIndividualReport([1,3]);
