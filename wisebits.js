'use strict';

/**
* @ngdoc function
* @name appApp.controller:SearchctrlCtrl
* @description
* # SearchctrlCtrl
* Controller of the appApp
*/
angular.module('music').controller('SearchCtrl', [
'$scope', '$http', '$timeout', 'MusicService', 'UserService', 'AjaxService', function(
    $scope,   $http,   $timeout,   MusicService,   UserService,   AjaxService
) {

    $scope.album_id = '';

    $scope.model = {
        tracks    : [],
        myTracks  : [],
        page_size : 50,
        query     : "",
        page      : 0,
        srchProc  : false, // в процессе поиска (отображать spinner)
    };

    $scope.init = function(album_id){
        $scope.album_id = album_id || '';
        MusicService.init();
    };

    $scope.search = function(){

        //$scope.model.srchProc = true;
        // Поиск в моей музыке (сразу всё)
        $http({
            method: 'GET',
            url: '/post/full-text-search?'+ $.param({
                q : $scope.model.query,
                params : { type  : "music" }
            }),
            headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data, status, headers, config) {
            if(data.error !== 0){
//                    $scope.$apply(function () {$scope.model.srchProc = false;});
                alert( "Request failed: " + data.error );
                return;
            }

            //console.log(data.result);

            $scope.model.myTracks = data.result;

            var page = $scope.model.page;

            //$scope.$apply(function () {$scope.model.srchProc = false;});
            if(data.result.length < $scope.model.page_size){
                // Поиск в soundcloud
                $scope.searchOnSoundCloud();
                ++page;
            }
            // Добавить событие на прокрутку.
            $(document).on('scroll', function(){
                var pxToEndPage = parseInt($(document).height() - ($(window).scrollTop() + $(window).height()));
                if($scope.model.tracks.length > 0 || $scope.model.myTracks.length > 0){
                    if(pxToEndPage < 330 && page === $scope.model.page){ // 330 - количество пикселей до конца страницы, при которой начинается автоподгрузка
                        $scope.searchOnSoundCloud();
                        ++page;
                    }
                    return false;
                }
            });

        }).error(function(data, status, headers, config) {
            //$scope.$apply(function () {$scope.model.srchProc = false;});
            alert( "Request failed: " + status );
        });
    };

    $scope.onAddTrackButtonClick = function($event, index, item, uid) {

        //items_add
        $http({
            method: 'POST',
            url: '/post/add-item',
            data: $.param({"type":'music', "url":item.items[0].url, "album_id":$scope.album_id, "uid":uid}), //sk: 30 Jan 2015: added uid when working on communities
            headers: {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).success(function(data, status, headers, config) {
            if(data.error === "0") {
                $timeout(function() {
                    $scope.model.tracks[index].id = data.result.id;
                    $scope.model.tracks[index].added = 1; // show the checkbox - we could do it by ''!==item.id, but currently a separate variable has been added
                    $scope.$digest();
                });
            } else {
                alert("Запрос не был выполнен: " + data.error);
            }
        }).error(function(data, status, headers, config) {
            alert( "Запрос не был выполнен: " + status );
        });

    }

    /**
     * Обрабатывает ввод и решает, делать поиск или нет
     *
     * @param {type} $event
     * @returns {undefined}
     */
    $scope.onChangeSearchQuery = function($event){

        var searchLine = $($event.target).parents('div.search-line');

        if($scope.model.query.length > 0){
            searchLine.find('button.search-line__btn').removeClass('btn-default');
            searchLine.find('button.search-line__btn-cancel').show();
            // Отключить живой поиск. При нажатии на enter осуществить поиск
            if($event.type === "keyup" && $event.keyCode !== 13){
                return;
            }

            $scope.model.tracks   = [];
            $scope.model.myTracks = [];
            $scope.search($scope.model.query);

            // Скрыть текущую страницу
            $('#content > section').hide();
        }
        else{
            $scope.onCloseSearch($event);
        }
    };

    $scope.onCloseSearch = function($event){
        var searchLine = $($event.target).parents('div.search-line');
        $scope.model.tracks   = [];
        $scope.model.myTracks = [];
        $scope.model.query    = "";
        $scope.model.page     = 0;

        $('#content > section').show();
        searchLine.find('button.search-line__btn').addClass('btn-default');
        searchLine.find('button.search-line__btn-cancel').hide();

    };
    // Пока сюда...
    var normTime = function( msec ){
        var result = '';
        var h, m, s;

        s = parseInt(msec/1000);

        h = s/3600|0;
        m = s/60|0;
        s = s%60;

        if(h > 0){
            result = h.toString() + ':'
            m = s%60;
            if(m < 10){
                m = '0' + m.toString();
            }
        }
        result+= m.toString() + ':'

        if(s < 10){
            s = '0' + s.toString();
        }
        else{
            s = s.toString();
        }

        result+= s;

        return result;
    };
    $scope.searchOnSoundCloud = function(){

        $timeout(function () {
            $scope.model.srchProc = true;
        });
        var offset = $scope.model.page_size * $scope.model.page;

        SC.get('/tracks', {
            q      : $scope.model.query,
            limit  : $scope.model.page_size,
            offset : offset,
            filter : 'streamable'
        }).then(function(tracks) {
            if(tracks.errors !== undefined) {
                $scope.$apply(function () {$scope.model.srchProc = false;});
                return;
            }
            $timeout(function () {
                angular.forEach(tracks, function(value, key){
                    if(!value.streamable){
                        return;
                    }
                    $scope.model.tracks.push({
                        'id': '',
                        'album_id': '',
                        'text': value.title,
                        'items': [{
                            'trackId': value.id,
                            'url': value.uri,
                            'duration': normTime(value.duration)
                        }]
                    });
                });
                if(tracks.length === $scope.model.page_size) {
                    ++$scope.model.page;
                }
                $scope.model.srchProc = false;
            });
        });
    };


    /**
     * Действия, которые будут доступны для моей музыки в поиске
     * удалять
     * перемещать между альбомами
     *
     * Если я в сообществе, то могу добавлять себе
     */
    $scope.myMusicPostActions = ['moveToAlbum', 'remove'];

    (function (){
        var ownerData = UserService.getOwnerData();
        if(ownerData && ownerData.type === 'community'){
            $scope.myMusicPostActions.push('copyToAlbum');
        }
    })();

    $scope.onRemoveMyMusicPost = function(postId)
    {
        for(var i in $scope.model.myTracks){
            if($scope.model.myTracks[i].id === postId){
                $scope.model.myTracks.splice(i, 1);
            };
        }
    };

    $scope.addMusic = function(track, promise){
        console.log(arguments);
        if(!track){
            alert('error. track params in undefined');
            console.log('error. track params in undefined');
        }

        var ownerData = UserService.getOwnerData();
        if(ownerData && ownerData.type === 'community'){
            var uid = ownerData.id;
        } else {
            var user = UserService.getUserData();
            var uid = user.id;
        }
        var params = {
            type:     'music',
            url:      track.uri,
            album_id: $scope.album_id,
            uid:      uid
        };

        console.log('add items params: ', params);
        AjaxService.post('post/add-item', params).then(function(data) {
            if(data.error === "0") {
                promise.resolve();
            } else {
                promise.reject();
            }
        }, function(data, status, headers, config) {
            promise.reject();
        });
    };
}]);
